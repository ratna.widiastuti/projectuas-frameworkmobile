# projectUAS-FrameworkMobile



## Client ~ Frontend
Untuk memulai dengan menjalankan perintah   :
- ``` npm run serve ```
- Kemudian chek browser dengan url http://localhost:8081

## Server ~ Backend
Untuk memulai dengan menjalankan perintah   :
- ``` php spark serve --port 8081 ```
- Kemudian aktifkan Apache dan MySQL di xampp, lalu jalankan dibrowser localhost\phpmyadmin\
- Kemudian buka tab baru jalankan codeigniter
