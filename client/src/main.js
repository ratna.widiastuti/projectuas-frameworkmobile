//import Vue from 'vue';
import { createApp } from 'vue'
//import VueRouter from 'vue-router';
//import { routes } from './routes';
import App from './App.vue'
//import router from './router'
import axios from 'axios'
import router from './router/index.js'

//Vue.use(BootstrapVue);
//Vue.use(VueRouter);

axios.defaults.baseURL = 'http://localhost:8080/'
const app = createApp(App)
app.use(router);
app.mount('#app')

//const router = new VueRouter({
//    routes
//  });

//createApp(App).use(router).mount('#app')
//new Vue({
//    router,
//    render: h => h(App)
//    }).$mount("#app");
